<?php

/**
 * @file
 *   Follow module's install and uninstall code.
 */

/**
 * Implements hook_install().
 */
function follow_install() {
  // Add a default link to this site's node RSS feed.
  db_insert('follow_links')
    ->fields(array(
      'name' => 'this-site',
      'path' => 'rss.xml',
      'options' => 'a:0:{}',
      'uid' => 0,
      'weight' => 0,
    ))
    ->execute();
  // Save the initial CSS.
  follow_save_css();
}

/**
 * Implements hook_schema().
 */
function follow_schema() {
  $schema['follow_links'] = array(
    'description' => 'Stores sitewide and user follow links.',
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique identifier for the {follow_links}.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => "The machine name of the {follow_links}.",
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "User's {users} uid.  Sitewide {follow_links} use uid 0",
      ),
      'path' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The Drupal path or extenal URL the {follow_links} should point to.',
      ),
      'options' => array(
        'description' => 'A serialized array of options to be passed to the url() or l() function, such as a query string or HTML attributes.',
        'type' => 'text',
        'translatable' => TRUE,
        'serialize' => TRUE,
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The human readable name for the link.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'The weight of this {follow_links}.',
      ),
    ),
    'primary key' => array('lid'),
    'unique keys' => array(
      'uid_name' => array('uid', 'name'),
    ),
  );
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function follow_uninstall() {
  \Drupal::config('follow.settings')->clear('follow_user_block_title');
  \Drupal::config('follow.settings')->clear('follow_site_block_title');
  \Drupal::config('follow.settings')->clear('follow_site_block_user');
  \Drupal::config('follow.settings')->clear('follow_site_icon_style');
  \Drupal::config('follow.settings')->clear('follow_user_icon_style');
  \Drupal::config('follow.settings')->clear('follow_site_alignment');
  \Drupal::config('follow.settings')->clear('follow_user_alignment');
}
